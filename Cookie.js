class Cookie {
	get(name) {
		const cname = `${name}=`;
		const decodedCookie = decodeURIComponent(document.cookie);
		const ca = decodedCookie.split(';');

		for (let i = 0; i < ca.length; i++) {
			let cookie = ca[i];

			while (cookie.charAt(0) === ' ') {
				cookie = cookie.substring(1);
			}

			if (cookie.indexOf(cname) === 0) {
				return cookie.substring(cname.length, cookie.length);
			}
		}

		return '';
	}

	set(name, value, expireDays = 1000) {
		const now = new Date();

		now.setTime(now.getTime() + expireDays * 24 * 60 * 60 * 1000);

		const expires = `expires=${now.toUTCString()}`;

		document.cookie = `${name}=${value};${expires};path=/;secure`;
	}
}

export default new Cookie();
